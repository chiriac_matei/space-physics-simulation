import itertools
import math
import pygame
import pymunk
import pymunk.pygame_util
from Planet import Planet

gravitational_constant = 6.67/1e11
(width, height) = (800, 600)
backgroundColor = (255, 255, 255)

win = pygame.display.set_mode((width, height))
pygame.display.set_caption("Planets")

space = pymunk.Space()
pymunk.pygame_util.positive_y_is_up = False
options = pymunk.pygame_util.DrawOptions(win)

planets = []
planets.append(Planet(100, width/2, height/2, space))
planets.append()


def distance(a, b):
    return math.sqrt((a[0] - b[0]) * (a[0] - b[0]) + (a[1]-b[1]) * (a[1]-b[1]))


def add_force_to_planets():
    for pair in itertools.combinations(planets, 2):
        dist = distance(pair[0].body.position, pair[1].body.position)
        fg = gravitational_constant * pair[0].body.mass * pair[1].body.mass / (dist * dist)
        print(fg)
        pair[0].attract_to(pair[1].body.position, fg)
        pair[1].attract_to(pair[0].body.position, fg)


def draw_planets():
    space.debug_draw(options)


running = True

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
            break

    add_force_to_planets()
    draw_planets()

    pygame.display.update()


