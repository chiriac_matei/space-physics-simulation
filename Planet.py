import pymunk

density = 1


class Planet:
    def __init__(self, radius, x, y, space):
        self.body = pymunk.Body()
        self.body.position = x, y
        self.shape = pymunk.Circle(self.body, radius)
        self.shape.density = density
        space.add(self.body, self.shape)

    def attract_to(self, target, power):
        pass


